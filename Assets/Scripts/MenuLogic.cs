﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class MenuLogic : MonoBehaviour {

    public static MenuLogic Instance;

    // MENU
    public GameObject HudPanel;
    public GameObject GameOverPanel;

    void Awake()
    {
        Instance = this;
    }

    // Use this for initialization
    void Start () {
        SetInGameUI();
    }

    public void SetInGameUI()
    {
        HudPanel.SetActive(true);
        GameOverPanel.SetActive(false);
    }

    public void SetGameOverUI()
    {
        HudPanel.SetActive(false);
        GameOverPanel.SetActive(true);
    }

    public void OnPlayNewGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
