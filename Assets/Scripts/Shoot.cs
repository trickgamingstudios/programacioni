﻿using UnityEngine;
using System.Collections;

public class Shoot : MonoBehaviour {

    public float TimeBetweenShots = 3.0f;
    public GameObject Bullet;
    public GameObject BulletSpawnPoint;
    public float speed = 20;

    private Transform _target;

	// Use this for initialization
	void Start () {
        _target = GameObject.FindGameObjectWithTag("Player").transform;
        StartCoroutine(ShootIn(TimeBetweenShots));
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    private IEnumerator ShootIn(float TimeBetweenShots) {
        yield return new WaitForSeconds(TimeBetweenShots);
        var shot = Instantiate(Bullet, BulletSpawnPoint.transform.position, Bullet.transform.rotation) as GameObject;

        shot.transform.LookAt(_target);

        var rb = shot.GetComponent<Rigidbody>();
        rb.velocity = (_target.position - transform.position).normalized * speed;

        StartCoroutine(ShootIn(TimeBetweenShots));
    }
}
