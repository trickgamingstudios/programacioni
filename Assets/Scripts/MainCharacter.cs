﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MainCharacter : MonoBehaviour {

    public int Health;
    public Text HealthUIText;

	// Use this for initialization
	void Start () {
        Health = 5;
	}
	

    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag == "Bullet")
        {
            Health -= 1;
            Destroy(other.gameObject);
            UpdateHealth();

            if (Health <= 0)
                GameLogic.Instance.GameOver();
        }
    }

    private void UpdateHealth() {
        HealthUIText.text = Health.ToString();
    }
}
