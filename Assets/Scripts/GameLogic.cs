﻿using UnityEngine;
using System.Collections;

public class GameLogic : MonoBehaviour {

    public static GameLogic Instance;

    void Awake()
    {
        Instance = this;
        Time.timeScale = 1.0f;
    }

    public void GameOver() {
        var character = GameObject.FindGameObjectWithTag("Ethan");
        character.SetActive(false);

        Time.timeScale = 0.0f;

        MenuLogic.Instance.SetGameOverUI();
    }
}
